/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	// "time"

	"github.com/go-logr/logr"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"

	capiv1 "sigs.k8s.io/cluster-api/api/v1alpha3"

	vsphere "sigs.k8s.io/cluster-api-provider-vsphere/api/v1alpha3"

	"gitlab.com/incubateurpe/ipam-operator/ipam"
)

const ipamFinalizer = "ipam-vsphere/finalizer"

// VSphereMachineReconciler reconciles a VSphereMachine object
type VSphereMachineReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	Config *ipam.Config
}

//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=vspheremachines,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=vspheremachines/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=vspheremachines/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.9.2/pkg/reconcile
func (r *VSphereMachineReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	reqLogger := log.FromContext(ctx)
	reqLogger.Info("Reconciling VSphereMachine " + req.NamespacedName.Namespace + "/" + req.NamespacedName.Name)

	// Fetch the &vsphere.VSphereMachine instance
	machine := &vsphere.VSphereMachine{}
	err := r.Get(ctx, req.NamespacedName, machine)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			reqLogger.Info("VSphereMachine resource not found. Ignoring since object must be deleted.")
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		reqLogger.Error(err, "Failed to get VSphereMachine.")
		return ctrl.Result{}, err
	}

	// Check if the VSphereMachine instance is marked to be deleted.
	isMarkedToBeDeleted := machine.GetDeletionTimestamp() != nil
	if isMarkedToBeDeleted {
		reqLogger.Info("VSphereMachine marked for deletion, removing ip reservations")
		if controllerutil.ContainsFinalizer(machine, ipamFinalizer) {
			if controllerutil.ContainsFinalizer(machine, vsphere.MachineFinalizer) {
				reqLogger.Info("Machine still in finalization by capv, requeing")
				return ctrl.Result{Requeue: true}, nil
			}

			reference := client.MergeFrom(machine.DeepCopy())
			// Run finalization logic for ipamFinalizer. If the finalization logic fails,
			// don't remove the finalizer so that we can retry during the next reconciliation.
			if err := r.finalizeMachine(reqLogger, machine); err != nil {
				return ctrl.Result{}, err
			}

			reqLogger.Info("All IP reservations removed successfully, removing finalizer")
			controllerutil.RemoveFinalizer(machine, ipamFinalizer)
			err := r.Patch(ctx, machine, reference)
			if err != nil {
				reqLogger.Error(err, "Failed to remove finalizer")
				return ctrl.Result{}, err
			}
		}
		return ctrl.Result{}, nil
	}

	//Check if machine is in WaitingForStaticIPAllocation status and reserve an IP.
	readyCondition := FindStatusCondition(machine.Status.Conditions, "Ready")
	if readyCondition != nil && readyCondition.Status == corev1.ConditionFalse &&
		readyCondition.Reason == vsphere.WaitingForStaticIPAllocationReason {

		reqLogger.Info("VSphereMachine in WaitingForStaticIPAllocation state, assigning IPs...")

		reference := client.MergeFrom(machine.DeepCopy())

		// Reserve and assign IP addresses to interfaces
		if err := r.AssignIpAddresses(reqLogger, machine); err != nil {
			reqLogger.Error(err, "Failed to get Ip addr")
			return ctrl.Result{}, err
		}

		// Add finalizer for this CR
		if !controllerutil.ContainsFinalizer(machine, ipamFinalizer) {
			controllerutil.AddFinalizer(machine, ipamFinalizer)
		}

		err = r.Patch(ctx, machine, reference)
		if err != nil {
			reqLogger.Error(err, "Failed to patch object with IpAdresses and finalizer")
			if err := r.finalizeMachine(reqLogger, machine); err != nil {
				reqLogger.Error(err, "Failed to remove IP reservations, aborting reconcilation")
				return ctrl.Result{}, nil
			}
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, nil
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *VSphereMachineReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&vsphere.VSphereMachine{}).
		WithOptions(controller.Options{MaxConcurrentReconciles: 1}).
		Complete(r)
}

func (r *VSphereMachineReconciler) AssignIpAddresses(reqLogger logr.Logger, machine *vsphere.VSphereMachine) error {
	machineName := machine.ObjectMeta.Name

	for indexDevice, device := range machine.Spec.Network.Devices {
		ipName := machineName
		if device.DeviceName != "" {
			ipName += "-" + device.DeviceName
		} else {
			ipName += "-" + strconv.Itoa(indexDevice)
		}

		if len(device.IPAddrs) == 0 {
			ips, err := r.Config.Ipam.GetIP(machine.ObjectMeta.Name)
			if err != nil {
				return err
			}
			for indexIp, ip := range ips {
				ips[indexIp] = ip + "/24"
				reqLogger.Info(fmt.Sprintf("Reserved IP %s", ip))
			}
			machine.Spec.Network.Devices[indexDevice].IPAddrs = ips
		}
	}

	return nil
}

func (r *VSphereMachineReconciler) finalizeMachine(reqLogger logr.Logger, machine *vsphere.VSphereMachine) error {
	for _, device := range machine.Spec.Network.Devices {
		if len(device.IPAddrs) != 0 {
			for _, ip := range device.IPAddrs {
				reqLogger.Info(fmt.Sprintf("Removing IP %s", ip))
				if err := r.Config.Ipam.DeleteIp(strings.TrimSuffix(ip, "/24")); err != nil {
					return err
				}
			}
		}
	}

	return nil
}

// FindStatusCondition finds the conditionType in conditions.
func FindStatusCondition(conditions []capiv1.Condition, conditionType capiv1.ConditionType) *capiv1.Condition {
	for i := range conditions {
		if conditions[i].Type == conditionType {
			return &conditions[i]
		}
	}

	return nil
}
