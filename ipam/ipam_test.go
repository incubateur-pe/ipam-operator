package ipam

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIpPoolList(t *testing.T) {
	server := createServer("../tests/ipam/pools.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.Ipam.SolidServer.IpPoolList("", 1)

	assert.Equal(t, "pool1", result[0]["pool_name"])
}

func TestIpFindFreeOneIPNoBeginEnd(t *testing.T) {
	server := createServer("../tests/ipam/ip_find_free_1.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.Ipam.SolidServer.IpFindFree("1")

	assert.Equal(t, "10.209.16.152", result[0]["hostaddr"])
}

func TestIpAddNoName(t *testing.T) {
	server := createServer("../tests/ipam/ip_add.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.Ipam.SolidServer.IpAdd("SITE TEST", "10.10.10.10", "")

	assert.Equal(t, "1", result[0]["ret_oid"])
}

func TestIpAddressListWithNameOnly(t *testing.T) {
	server := createServer("../tests/ipam/ip_address_list.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.Ipam.SolidServer.IpAddressList("test-tdd.sii24.pole-emploi.intra", "", 1)

	assert.Equal(t, "test-tdd.sii24.pole-emploi.intra", result[0]["name"])
}

func TestIpAddressListWithIpOnly(t *testing.T) {
	server := createServer("../tests/ipam/ip_address_list.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.Ipam.SolidServer.IpAddressList("", "10.10.10.10", 1)

	assert.Equal(t, "test-tdd.sii24.pole-emploi.intra", result[0]["name"])
}

func TestIpDelete(t *testing.T) {
	server := createServer("../tests/ipam/ip_delete.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.Ipam.SolidServer.IpDelete("id")

	assert.Equal(t, "1", result[0]["ret_oid"])
}

func getConfig(url string) Config {
	config := Config{}
	config.Ipam.SolidServer.Url = url
	config.Ipam.SolidServer.SkipSslValidation = true

	return config
}

func createServer(file string) *httptest.Server {
	bytes, _ := ioutil.ReadFile(file)
	// Start a local HTTP server
	return httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write(bytes)
	}))
}
