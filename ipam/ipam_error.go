package ipam

import (
	"fmt"
)

type IpAlreadyExists struct {
	Ip   string
	Name string
}

func (e IpAlreadyExists) Error() string {
	return fmt.Sprintf("IP %s, already reserved with name %s", e.Ip, e.Name)
}
