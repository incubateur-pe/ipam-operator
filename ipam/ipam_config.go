package ipam

import (
	"errors"
	"io/ioutil"
	"strings"

	"github.com/ghodss/yaml"
)

type Config struct {
	Ipam Ipam `yaml:"ipam"`
}

type Ipam struct {
	SolidServer SolidServer `yaml:"solidServer"`
	Pool        string      `yaml:"pool"`
	SiteName    string      `yaml:"siteName"`
}

type SolidServer struct {
	Url               string `yaml:"url"`
	SkipSslValidation bool   `yaml:"SkipSslValidation"`
}

func LoadConfig(configFile string) (*Config, error) {
	data, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, err
	}

	var cfg Config
	if err := yaml.Unmarshal(data, &cfg); err != nil {
		return nil, err
	}

	cfg.Ipam.SolidServer.Url = strings.TrimSuffix(cfg.Ipam.SolidServer.Url, "/")

	// Verify mandatory parameters
	if cfg.Ipam.SolidServer.Url == "" {
		return nil, errors.New("Config error : SolidServer url is mandatory")
	}
	if cfg.Ipam.Pool == "" || cfg.Ipam.SiteName == "" {
		return nil, errors.New("Config error : SiteName and pool are mandatory")
	}
	return &cfg, nil
}
