package ipam

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfigLoader(t *testing.T) {
	config, err := LoadConfig("../tests/config-vspheremachine-controller.yaml")

	if err != nil || config == nil {
		t.Error(err)
	}

	assert.Equal(t, "testSiteName", config.Ipam.SiteName, "SiteName should be 'testSiteName'")
	assert.Equal(t, "testPool", config.Ipam.Pool, "Pool should be 'testPool'")

	assert.Equal(t, "https://testHost", config.Ipam.SolidServer.Url, "SolidServer host should be 'https://testHost'")
	assert.False(t, config.Ipam.SolidServer.SkipSslValidation, "SolidServer SkipSslValidation should be false")
}
