package ipam

import (
	"fmt"
	"strings"
)

type Where struct {
	Conditions    []string
	NextOperation string
}

func (w *Where) Condition(criterion string, value string) *Where {
	if value != "" {
		if w.NextOperation != "" {
			w.Conditions = append(w.Conditions, w.NextOperation)
		}
		w.Conditions = append(w.Conditions, fmt.Sprintf("%s='%s'", criterion, value))
	}

	w.NextOperation = ""

	return w
}

func (w *Where) Or() *Where {
	w.NextOperation = " OR "
	return w
}

func (w *Where) And() *Where {
	w.NextOperation = " AND "
	return w
}

func (w Where) String() string {
	if len(w.Conditions) != 0 {
		if w.Conditions[0] == " AND " || w.Conditions[0] == " OR " {
			w.Conditions = w.Conditions[1:]
		}
	}
	return strings.Join(w.Conditions, "")
}
