package ipam

import (
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
)

func (i Ipam) GetIP(name string) ([]string, error) {
	ipList, err := i.SolidServer.IpAddressList(name, "", 1)
	if err != nil {
		return []string{}, err
	}
	if len(ipList) != 0 {
		return []string{}, IpAlreadyExists{Name: name, Ip: ipList[0]["hostaddr"]}
	}

	pool, err := i.SolidServer.IpPoolList(i.Pool, 1)
	if err != nil {
		return []string{}, err
	}
	ipFree, err := i.SolidServer.IpFindFree(pool[0]["pool_id"])
	if err != nil {
		return []string{}, err
	}
	ip := ipFree[0]["hostaddr"]

	if _, err := i.SolidServer.IpAdd(i.SiteName, ip, name); err != nil {
		return []string{}, err
	}

	return []string{ip}, nil
}

func (i Ipam) DeleteIp(ip string) error {
	ipList, err := i.SolidServer.IpAddressList("", ip, 1)
	if err != nil {
		return err
	}

	if len(ipList) == 1 {
		_, err := i.SolidServer.IpDelete(ipList[0]["ip_id"])
		if err != nil {
			return err
		}
	}
	return nil
}

func (s SolidServer) request(method string, ipamCommand string, queryString map[string]string) (*http.Response, error) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: s.SkipSslValidation},
	}
	client := &http.Client{Transport: tr}

	request, err := http.NewRequest(method, s.Url+"/"+ipamCommand, nil)
	if err != nil {
		return nil, err
	}

	u := request.URL
	query := u.Query()
	for key, value := range queryString {
		query.Add(key, value)
	}
	u.RawQuery = query.Encode()

	request.Header = map[string][]string{
		"X-IPM-Username": {base64.StdEncoding.EncodeToString([]byte(os.Getenv("IPAM_USERNAME")))},
		"X-IPM-Password": {base64.StdEncoding.EncodeToString([]byte(os.Getenv("IPAM_PASSWORD")))},
	}

	return client.Do(request)
}

func (s SolidServer) IpPoolList(poolName string, limit int) ([]map[string]string, error) {
	var where = Where{}
	var queryString = map[string]string{
		"WHERE": fmt.Sprintf("%s", where.Condition("pool_name", poolName)),
		"limit": strconv.Itoa(limit)}

	response, err := s.request("GET", "rest/ip_pool_list", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()
	return result, nil
}

// TODO : remonter une erreur si aucune IP trouvée
func (s SolidServer) IpFindFree(poolId string) ([]map[string]string, error) {
	var queryString = map[string]string{"pool_id": poolId, "max_find": "1"}

	response, err := s.request("GET", "rpc/ip_find_free_address", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()
	return result, nil
}

func (s SolidServer) IpAdd(siteName string, ip string, name string) ([]map[string]string, error) {
	var queryString = map[string]string{
		"site_name": siteName,
		"hostaddr":  ip,
		"name":      name}

	response, err := s.request("POST", "rest/ip_add", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()
	return result, nil
}

func (s SolidServer) IpAddressList(name string, ip string, limit int) ([]map[string]string, error) {
	var where = Where{}
	var queryString = map[string]string{
		"WHERE": fmt.Sprintf("%s", where.Condition("name", name).Or().Condition("hostaddr", ip)),
		"limit": strconv.Itoa(limit)}

	response, err := s.request("GET", "rest/ip_address_list", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()
	return result, nil
}

func (s SolidServer) IpDelete(ipId string) ([]map[string]string, error) {
	var queryString = map[string]string{"ip_id": ipId}

	response, err := s.request("DELETE", "rest/ip_delete", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()

	return result, nil
}
